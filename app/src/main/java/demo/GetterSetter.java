package demo;

import lombok.Getter;
import lombok.Setter;

public class GetterSetter {
    @Getter
    @Setter
    private static String username;

    @Getter
    @Setter
    private String password;

    public static void main(String[] args){
        username="Arji";
        System.out.println(getUsername());
    }
}
